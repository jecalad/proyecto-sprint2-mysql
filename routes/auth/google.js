const express = require('express')
const passport = require('passport')
const GoogleStrategy = require('passport-google-oauth').OAuth2Strategy

const router = express.Router()

passport.serializeUser((user, done) => {
    done(null, user)
})
  
passport.deserializeUser((obj, done) => {
    done(null, obj)
})
  

passport.use(new GoogleStrategy({
  clientID: '363330036623-ai0ludmlqgguk17oueq1lmmjafaqv9qa.apps.googleusercontent.com',
  clientSecret: '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',
  callbackURL: 'http://localhost:3000/google/callback'
}, (accessToken, refreshToken, profile, done) => {
  return done(null, profile)
}))

router.get('/google', passport.authenticate('google', { scope: ['profile', 'email']}))

router.get(
  '/google/callback', 
  passport.authenticate('google', { successRedirect: '/api-docs', failureRedirect: '/' }))

module.exports = router
