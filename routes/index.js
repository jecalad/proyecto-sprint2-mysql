const router = require('express').Router();


const UserRoutes = require('./usuarios.routes');
const PerfilRoutes = require('./perfil.routes');
const ProductsRoutes = require('./productos.routes');
const PedidosRoutes = require('./pedidos.routes');
const LoginRoutes = require('./login.routes');
const MetodosPagoRoutes = require('./metodosPagos.routes');
const closeOrderRoutes = require('./closeOrder.routes');
const googleAuthRouter = require('./auth/google');
const auth0Router = require('./auth/auth0');

//middleware
const {validarToken} = require('../middleware/login/login.middleware');

//login
router.use('/login', LoginRoutes);

// list, create , update and delete users
router.use('/usuarios', UserRoutes);

//update user profile only
router.use('/perfil', validarToken, PerfilRoutes);

//list, create, update and delete Products
router.use('/productos', ProductsRoutes);

//create orders
router.use('/pedidos', validarToken, PedidosRoutes);

//crear, read,update and delete Payment Methods
router.use('/pagos',  MetodosPagoRoutes);

//cerrar ordenes
router.use('/closeOrder', closeOrderRoutes);

router.use('/login/google', googleAuthRouter);

router.use('/login/auth0', auth0Router);

/* GET home page. */
router.get('/', function (req, res, next) {
    res.render('index', { title: 'Auth0 Webapp sample Nodejs' });
});

module.exports = router;
