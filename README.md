Proyecto ACAMICA Sprint 2 - App Persistente

Prerrequisitos:
    * MYSQL
    * git
    * nodeJS

Instrucciones de instalacion:

    1. Crear folder
    2. ejecutar git clone https://gitlab.com/jecalad/proyecto-sprint2-mysql.git
    3. ingresar a la carpeta de la aplicacion y ejecutar "npm install"
    4. Ir al archivo en la carpeta src/db.config.js e ingresar los datos de conexión a la BD
    5. Crear un archivo de variables de entorno .env con las siguientes variables:
        * EXPORT = Your_APP_listening_port
        * JWT_SECRET = "Your_JWT_secret"
    6. ejecutar "node src/app.js"


DOCUMENTACION SWAGGER:

Para probar los endpoint primero debes crear un usuario usuando el endpoint de post en la seccion usuarios y luego hacer login usando el endpoint de inicio de sesion para obtener un token JWT.

    * http://localhost:3000/api-docs

Testing:

    * en la carpeta test se encuentran las rutinas de testing.
    