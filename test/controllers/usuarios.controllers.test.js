const Usuario = require('../../models/user.models');
const Perfiles = require('../../models/perfil.models');

const { listarUsuarios, createUser } = require('../../controllers/usuarios.controller');

const chai = require('chai');
const expect = chai.expect;
const faker = require('faker');
const sinon = require('sinon');

describe('validar controller usuarios',()=>{
    let expectedStubValue;
    let stubValue;
    // let userRepository;
    // let userService;

    let createRequestStubValue;
    let createStubValue;

    let createRequestPerfilStubValue;
    let createPerfilStubValue;

    let user;

    describe('/post usuarios', ()=>{

      before(function(){
        user =  {
          "id": 1,
          "nombre": "admin",
          "apellido": "admin",
          "email": "admin@gmail.com",
          "fecha_creacion": "2021-08-10T01:35:39.000Z",
          "admin": false,
          "activo": true,
          "perfil": {
            "id": 1,
            "nombre_usuario": "admin",
            "contrasena": "0123456789",
            "direccion": "cra64C348-56",
            "pais": "Colombia",
            "telefono": 3122915658,
            "user_id": 1
          }
        }
      });

      it('crear un usuario', async()=>{
        // const stub = sinon.stub(Usuario, "create").returns(createStubValue);
        // const perfilStub = sinon.stub(Perfiles, "create").returns(createRequestPerfilStubValue);
        //const setPerfilStub = sinon.stub(Usuario, "setPerfil").returns([]);
        console.log(user.nombre);
        const createdUser = await createUser(user.nombre, user.apellido, user.email, user.perfil.nombre_usuario, user.perfil.contrasena);

        expect(createdUser.id).to.equal(user.id);
        expect(createdUser.email).to.equal(user.email);
      })

    })
  })
    // describe.skip('/get usuarios', async()=>{

    //     // before(function () {
    //     //     userRepository = new UserRepository();
    //     //     userService = new UserService(userRepository);
    //     // });

    //     before(function () {
    //         expectedStubValue = [{
    //           id: faker.datatype.number(),
    //           nombre: faker.name.findName(),
    //           apellido: faker.name.firstName(),
    //           email: faker.internet.email(),
    //           fecha_creacion: faker.date.past(),
    //           admin: faker.datatype.boolean(),
    //           activo: faker.datatype.boolean(),
    //         }];
    //         stubValue = expectedStubValue;
    //       });

    //     it('listar todos los usuarios', async()=>{
    //         const stub = sinon.stub(Usuario, "findAll").returns(stubValue);

    //         const usuarios = await listarUsuarios();
            
    //         expect(stub.calledOnce).to.be.true;
    //         expect(usuarios[0].id).to.equal(expectedStubValue[0].id);

    //     })
    // })

    




    // {
    //   "fecha_creacion": 1641334620439,
    //   "admin": false,
    //   "activo": true,
    //   "id": 4,
    //   "nombre": "abdon",
    //   "apellido": "profe",
    //   "email": "aprofe@gmail.com",
    //   "perfil": {
    //     "direccion": null,
    //     "pais": null,
    //     "telefono": null,
    //     "id": 3,
    //     "nombre_usuario": "abdon",
    //     "contrasena": "$2b$10$eQ0IVGkx5cxpKabWkRVVH.HN80C59yqZw5XiUhv8MlmazYPAcbT72",
    //     "user_id": 4
    //   }
    // }

    // describe('/post usuarios', async()=>{

    //   before(function () {
    //     expectedStubValue = [{
    //       id: faker.datatype.number(),
    //       nombre: faker.name.findName(),
    //       apellido: faker.name.firstName(),
    //       email: faker.internet.email(),
    //       fecha_creacion: faker.date.past(),
    //       admin: faker.datatype.boolean(),
    //       activo: faker.datatype.boolean(),
    //     }];
    //     stubValue = expectedStubValue;
    //   });

    //   it('crear un usuario', async()=>{
    //     const stub = sinon.stub(Usuario, "create").returns(stubValue);

    //     const usuarios = await listarUsuarios();
        
    //     expect(stub.calledOnce).to.be.true;
    //     expect(usuarios[0].id).to.equal(expectedStubValue[0].id);

    //   })
    // });
