const Pedidos = require('../../models/pedidos.model');

const jwt = require('jsonwebtoken');
require('dotenv').config();



async function validateUsuario(req, res, next){
    const { id } = req.params;

    try{
        
        const token = req.query.token;
        const validateToken = jwt.verify(token, process.env.JWT_SECRET);
        const usuario = validateToken.user_id

        const pedido = await Pedidos.findByPk(id);
        if(!pedido) return res.status(404).json({error: "pedido no encontrado"});

        if(pedido.usuario_id != usuario) return res.status(403).json({error: "No se puede eliminar este pedido"});

        next();
    }catch(err){
        return res.status(500).json({error: err});
    }

}

module.exports = { validateUsuario };